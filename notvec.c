#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define CHUNK 16

typedef void* vector;

typedef struct {
    size_t size;
    size_t cap;
    size_t nmemb;
} meta;

// creates a new empty vector, ready to hold elements of size nmemb
vector vec_new(size_t nmemb) {
    meta* head = malloc(sizeof(meta));
    head->size = 0;
    head->cap = 0;
    head->nmemb = nmemb;
    return head + 1;
}

// creates a new vector, ready to hold cap elements of size nmemb
vector vec_with_capacity(size_t nmemb, size_t cap) {
    meta* head = malloc(sizeof(meta) + cap * nmemb);
    head->size = 0;
    head->cap = cap;
    head->nmemb = nmemb;
    return head + 1;
}

// appends element pointed to by elem to the end of vector
// may move the vector to a new location
vector vec_push(vector vec, void* elem) {
    meta* head = ((meta*)vec) - 1;
    if (head->size >= head->cap) {
        size_t new_size = head->cap ? 2 * head->cap : CHUNK;
        head = realloc(head, sizeof(meta) + new_size * head->nmemb);
    }
    char* data = vec;
    memcpy(data + head->nmemb * head->size, elem, head->nmemb);
    head->size += 1;
    return head + 1;
}

// removes last element from vector and returns its address
// if vector is empty, returns NULL instead
// do note the address is guaranteed to be valid until next operation on vector
void* vec_pop(vector vec) {
    meta* head = ((meta*)vec) - 1;
    if (head->size == 0) {
        return NULL;
    }
    head->size -= 1;
    char* data = vec;
    return data + head->size * head->nmemb;
}

// returns vectors capacity
size_t vec_capacity(vector vec) {
    meta* head = ((meta*)vec) - 1;
    return head->cap;
}

// returns number of elements in vector
size_t vec_size(vector vec) {
    meta* head = ((meta*)vec) - 1;
    return head->size;
}

// calls predicate on all elements of vector
// removes all elements that evaluate to false
// if dropper is supplied, it is called on all elements that evaluated to false
void vec_retain(vector vec, bool (*predicate)(void*), void (*dropper)(void*)) {
    meta* head = ((meta*)vec) - 1;
    char* data = vec;
    size_t newsize = 0;
    for (size_t i = 0; i < head->size; i += 1) {
        if ((*predicate)(data + i * head->nmemb)) {
            memcpy(data + newsize * head->nmemb, data + i * head->nmemb, head->nmemb);
            newsize += 1;
        } else {
            if (dropper != NULL) {
                (*dropper)(data + i * head->nmemb);
            }
        }
    }
    head->size = newsize;
}

// calls fn on each element of vector
// fn must operate in-place
void vec_map(vector vec, void (*fn)(void*)) {
    meta* head = ((meta*)vec) - 1;
    char* data = vec;
    for (size_t i = 0; i < head->size; i += 1) {
        (*fn)(data + i * head->nmemb);
    }
}

// returns true if there are no elements in vector
// false otherwise
bool vec_is_empty(vector vec) {
    meta* head = ((meta*)vec) - 1;
    return head->size == 0;
}

// frees memory allocated by vector
// if a dropper is supplied, it will be called on all elements of the vector
// do not free vector directly, it WILL fault
void vec_drop(vector vec, void (*dropper)(void*)) {
    if (dropper != NULL) {
        vec_map(vec, dropper);
    }
    meta* head = ((meta*)vec) - 1;
    free(head);
}

// keeps first `at` elements, moves the rest to a new vector
// returns the new vector
// if `at` is greater than size of `vec`, returns NULL
vector vec_split_off(vector vec, size_t at) {
    meta* head = ((meta*)vec) - 1;
    if (at > head->size) {
        return NULL;
    }
    char* data = vec;
    vector new = vec_with_capacity(head->nmemb, head->size - at);
    memcpy(new, data + head->nmemb * at, (head->size - at) * head->nmemb);
    head->size = at;
    return new;
}

// copies all elements from `other` into `vec`
// returns the new vector
// if element size of `vec` and `other` does not match,
// the operation is canceled and NULL is returned instead
vector vec_append(vector vec, vector other) {
    meta* head = ((meta*)vec) - 1;
    meta* other_head = ((meta*)other) - 1;
    if (head->nmemb != other_head->nmemb) {
        return NULL;
    }
    size_t new_size = head->size + other_head->size;
    if (new_size > head->cap) {
        head = realloc(head, sizeof(meta) + new_size * head->nmemb);
        head->cap = new_size;
    }
    memcpy(vec + head->size * head->nmemb, other, other_head->size * other_head->nmemb);
    head->size = new_size;
    vec_drop(other, NULL);
    return head + 1;
}

//TODO:

//remove element at index, shift elements after index left
void vec_remove(vector vec, size_t index);

//insert element at index, shift all elements after it right
void vec_insert(vector* vec, size_t index, void* element);

//remove element at index, move last element to its place;
void vec_swap_remove(vector vec, size_t index);

//self-explanatory
void vec_grow_by(vector* vec, size_t);
